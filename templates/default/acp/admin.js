/**
 * Created by JetBrains PhpStorm.
 * User: werring
 * Date: 12/30/11
 * Time: 15:35:35
 */

$(document).ready(function(){

    openAdmin();
    $('#admin #block #close').click(function(){
        $(this).fadeOut(500);
        $('#admin #block').animate({
            height: '0px',
            top: '50%',
            paddingTop: 0,
            paddingBottom: 0
        },500,function(){
            $('#admin #block').animate({
                width: '0px',
                left: '50%',
                paddingLeft: 0,
                paddingRight: 0
            },500,function(){
                $('#admin #block').css('display','none');
                $('#admin').fadeOut(500);
            });
        });
    });

    $("#menuDelete").click(
        function(event){
            var del = confirm("Are you sure you want to delete this file?");
            if(!del) event.preventDefault();
        }
    );
});

function openAdmin(){
    $("#admin #block").removeAttr('style');
    $("#admin #block #close").removeAttr('style');
    $("#admin").fadeIn(1500);
}

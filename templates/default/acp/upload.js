/**
 * Created by JetBrains PhpStorm.
 * User: werring
 * Date: 12/30/11
 * Time: 17:19:50
 */
$(document).ready(function(){
    var uploader = new qq.FileUploader({
        element: document.getElementById('upload'),
        action: '?upload&mod=uploader',
        debug: true,
        onComplete: function(id, fileName, responseJSON){
            if(responseJSON.success){
                $('#links').append("<a href='"+responseJSON.url+"' target='_blank'>" + responseJSON.url + "</a><br/>");
            }
        }
    });
    setInterval(removeItems,2000);
});

function removeItems(){
    $("ul.qq-upload-list li.qq-upload-success").fadeOut(1000,function(){
        $(this).remove();
    });
}
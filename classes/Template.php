<?php
	/**
		* Created by Werring Webdevelopment.
		* User: werring
		* Date: 12/23/11
		* Time: 14:18:57
		*/
	final class Template
	{

		const begTMPL = '${';
		const endTMPL = '}';
		private $tmplName      = null;
		private $tmplLoc       = null;
		private $tmplContent   = array();
		private $tmplVars      = array();
		private $tmplHeader    = "";
		/**
		 * @param string $name
		 */
		public function __construct($name){
			$this->setTemplate($name);
			$this->setVar('template',$name);
		}

		/**
		 * @param string $name
		 */
		public function setTemplate($name){

			if(file_exists(__PATH__. 'templates/' . $name . '/main.html')){
				$this->tmplName = $name;
				$this->tmplLoc = __PATH__. 'templates/' . $name . '/';
				$this->tmplContent = $this->importFiles($this->tmplLoc);
			} else {
				echo "no " . __PATH__. 'templates/' . $name . '/main.html';
			}

		}
		/**
		 * @param $dir
		 * @return array
		 */
		private function importFiles($dir){
			$dirList = scandir($dir);
			$retArray = array();
			for($i =0; $i<count($dirList);$i++){
				if(is_dir($dir . $dirList[$i])) continue;
				$pInfo = pathinfo($dirList[$i]);
				if($pInfo['extension'] == 'html'){
					$retArray[$pInfo['filename']] = file_get_contents($dir . $dirList[$i]);
				}
			}
			return $retArray;
		}
		/**
		 * @param $name
		 * @param $value
		 */
		public function setVar($name,$value){
			$this->tmplVars[$name] = $value;

		}
		/**
		 * @param $templateVars
		 */
		public function fillTemplate($templateVars){
			if(is_array($templateVars)){
				foreach($templateVars as $key => $value){
					$this->setVar($key,$value);
				}
			}
		}
		/**
		 * @param string $tmpl
		 * @param bool $return
		 * @return mixed
		 */
		public function parse($tmpl = null,$return = false){
			if(is_null($tmpl)){
				foreach($this->tmplContent as $template => $val){
					if($template == 'main') continue;
					if(!isset($this->tmplVars[$template])){
						$this->setVar($template,$this->parse($template,true));
					}
				}
				$this->parse('main');
			}
			else
			{
				$this->setVar('header',$this->tmplHeader);

				$html = $this->tmplContent[$tmpl];
				foreach($this->tmplVars as $key => $value){
					$html = str_replace(self::begTMPL . $key . self::endTMPL,$value, $html);
				}
				if($return){
					return $html;
				} else {
					echo $html;
				}
			}
			return null;
		}

		/**
		 * @param string $path Path to file
		 * @param bool $rel Relative to template directory
		 */
		public function addPage($path,$rel=true)
		{
			if(!$rel) $filePath = $path;
			else $filePath = $this->tmplLoc . $path;

			if(file_exists($filePath)){
				$pInfo = pathinfo($filePath);
				if($pInfo['extension'] == 'html'){
					$this->tmplContent[$pInfo['filename']] = file_get_contents($filePath);
					unset($this->tmplVars[$pInfo['filename']]);
				}
			}
		}

		/**
		 * @param string $filePath
		 * @param bool $tmplDir
		 */
		public function addCSS($filePath,$tmplDir=true){
			$filePath = ($tmplDir) ? "/".str_replace(__PATH__,"",$this->tmplLoc) . $filePath : $filePath . "?raw";
			$this->tmplHeader .= "<link type=\"text/css\" media=\"screen\" rel=\"stylesheet\" href=\"{$filePath}\" />" . PHP_EOL;
		}
		/**
		 * @param string $filePath
		 * @param bool $tmplDir
		 */
		public function addJS($filePath,$tmplDir=true){
			$filePath = ($tmplDir) ? "/".str_replace(__PATH__,"",$this->tmplLoc) . $filePath : $filePath . "?raw";
			$this->tmplHeader .= "<script type=\"text/javascript\" src='{$filePath}' ></script>" . PHP_EOL;
		}

		public function deleteJsCss(){
			$this->tmplHeader = "";
		}

		/**
		 * @param      $tag
		 * @param      $string
		 * @param null $attr
		 *
		 * @return string
		 */public function wrapTag($tag,$string,$attr = null){
			return "<".$tag." ".$attr.">".$string."<" . $tag . ">";
		}
	}

<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/23/11
	 * Time: 21:24:05
	 */
	class Filetypes_PHP extends FileDisplay
	{
		/**
		 * display
		 * @access public
		 */
		public function display(){
			$this->tmplVars['txt'] = "true";
			$this->tmplVars['lbContent'] .= '<br /><p style="text-align: left; padding-left: 5px;">BBcode:<br/><textarea cols="25" rows="10" readonly="readonly">[code]' . file_get_contents($this->reg->Conf->File->pInfo['file']) . "[/code]</textarea></p>";
			$content = highlight_string(trim(file_get_contents($this->reg->Conf->File->pInfo['file'])),true);
			$this->tmplVars['content'] = $content;
		}
	}

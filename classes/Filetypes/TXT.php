<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/23/11
	 * Time: 20:02:28
	 */
	class Filetypes_TXT extends FileDisplay
	{
		/**
		 * display
		 * @access public
		 */
		protected function display(){
			$this->tmplVars['txt'] = "true";
			$content = htmlentities(file_get_contents($this->reg->Conf->File->pInfo['file']));
			$this->tmplVars['content'] = "<pre>".trim($content)."</pre>";
		}
	}

<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/23/11
	 * Time: 16:52:39
	 */
	final class Filetypes_HTML extends FileDisplay
	{
		/**
		 * display
		 * @access public
		 */
		public function display(){
			$this->tmplVars['txt'] = "true";
			if(isset($_GET['preview'])){
				$this->tmplVars['lbContent'] .= "<br/><a href='".$this->reg->Conf->File->pInfo['webPath']."'>Show source</a>";
				$this->tmplVars['content'] = "<iframe style='border: none;' class='maxsize' src='".$this->reg->Conf->File->pInfo['webPath']."?raw' ></iframe>";
			} else {
				$this->tmplVars['lbContent'] .= "<br/><a href='".$this->reg->Conf->File->pInfo['webPath']."?preview'>Show preview</a>";
				$this->tmplVars['content'] = "<pre>".$this->html_highlight(file_get_contents($this->reg->Conf->File->pInfo['file']))."</pre>";
			}
			$this->tmplVars['lbContent'] .= '<br/><br /><p style="text-align: left; padding-left: 5px;">BBcode:<br/><textarea cols="25" rows="10" readonly="readonly">[code]' . file_get_contents($this->reg->Conf->File->pInfo['file']) . "[/code]</textarea></p>";

		}

		/**
		 * html_hightlight
		 * @param string $s HTML
		 * @return string
		 * @public
		 */
		public function html_highlight($s)
		{
			$s = htmlspecialchars($s);
			$s = preg_replace("#&lt;([/]*?)(.*)([\s]*?)&gt;#sU",
				'<span style="color: #0000FF">&lt;\\1\\2\\3&gt;</span>',$s);
			$s = preg_replace("#&lt;([\?])(.*)([\?])&gt;#sU",
				'<span style="color:#800000">&lt;\\1\\2\\3&gt;</span>',$s);
			$s = preg_replace("#&lt;([^\s\?/=])(.*)([\[\s/]|&gt;)#iU",
				"&lt;<span style=\"color:#808000\">\\1\\2</span>\\3",$s);
			$s = preg_replace("#&lt;([/])([^\s]*?)([\s\]]*?)&gt;#iU",
				"&lt;\\1<span style=\"color:#808000\">\\2</span>\\3&gt;",$s);
			$s = preg_replace("#([^\s]*?)\=(&quot;|')(.*)(&quot;|')#isU",
				"<span style=\"color:#800080\">\\1</span>=<span style=\"color:#FF00FF\">\\2\\3\\4</span>",$s);
			$s = preg_replace("#&lt;(.*)(\[)(.*)(\])&gt;#isU",
				"&lt;\\1<span style=\"color:#800080\">\\2\\3\\4</span>&gt;",$s);
			return $s;
		}
	}

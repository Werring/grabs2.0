<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/23/11
	 * Time: 23:59:45
	 */
	class Filetypes_SWF extends FileDisplay
	{
		/**
		 * display
		 * @access public
		 */
		public function display(){
			$name = $this->reg->Conf->File->pInfo['basename'];
			$webPath = $this->reg->Conf->File->pInfo['webPath'] . "?raw";
			$url = 'http://'.$_SERVER['HTTP_HOST'].$webPath;
			$this->tmplVars['content'] = <<<HTML
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="550" height="400" id="$name" align="middle">
    <param name="movie" value="$url"/>
    <!--[if !IE]>-->
    <object type="application/x-shockwave-flash" data="$url" width="550" height="400">
        <param name="movie" value="$url"/>
    <!--<![endif]-->
        <a href="http://www.adobe.com/go/getflash">
            <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player"/>
        </a>
    <!--[if !IE]>-->
    </object>
    <!--<![endif]-->
</object>
HTML;





			$this->tmplVars['lbContent'] .= '<p style="text-align: left; padding-left: 5px;">BBcode:<br/><input type="text" size="28" readonly="readonly" value="[swf]'.$url.'[/swf]"/></p>';
			$this->tmplVars['lbContent'] .= '<p style="text-align: left; padding-left: 5px;">HTML:<br/><textarea cols="25" rows="10" readonly="readonly">';
			$this->tmplVars['lbContent'] .= htmlentities($this->tmplVars['content']);
			$this->tmplVars['lbContent'] .='</textarea></p>';
		}
	}

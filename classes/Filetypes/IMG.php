<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/23/11
	 * Time: 23:20:13
	 */
	class Filetypes_IMG extends FileDisplay
	{
		/**
		 * display
		 * @access public
		 */
		public function display(){
			$this->tmplVars['img'] = "true";
			$this->tmplVars['content'] = "<img src='".$this->reg->Conf->File->pInfo['webPath']."' />";
			$this->tmplVars['lbContent'] .= '<p style="text-align: left; padding-left: 5px;">BBcode:<br/><input type="text" size="28" readonly="readonly" value="[img]http://'.$_SERVER['HTTP_HOST']."/".$this->reg->Conf->File->pInfo['webPath'].'[/img]"/></p>';
			$this->tmplVars['lbContent'] .= '<p style="text-align: left; padding-left: 5px;">HTML:<br/><input type="text" size="28" readonly="readonly" value="<img src=\'http://'.$_SERVER['HTTP_HOST']."/".$this->reg->Conf->File->pInfo['webPath'].'\' />"/></p>';
		}
	}

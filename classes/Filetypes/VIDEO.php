<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 1/8/12
	 * Time: 14:50:26
	 */
	class Filetypes_VIDEO extends FileDisplay
	{
		public function display()
		{
			$this->tmplVars['txt'] = "true";
			$this->tmplVars['content'] = "<video controls>";
			$this->tmplVars['content'].= $this->sources($this->reg->Conf->File->pInfo);
			$this->tmplVars['content'].="Your browser doesn't support HTML5 video, please upgrade your browser.</video>";
		}

		/**
		 * @param $file
		 * @return string
		 */
		private function sources($file){
			$vidExt = array('ogv','webm','mp4');
			$vidType = array('video/ogg; codecs="theora, vorbis"','video/webm; codecs="vp8.0, vorbis"','video/mp4; codecs="avc1.4D401E, mp4a.40.2"');

			$fileNoExt = $file['dirname'] . "/" . $file['filename'] . ".";

			$source = "";
			for($i = 0;$i<count($vidExt);$i++){

				if(!file_exists($fileNoExt . $vidExt[$i])) continue;
				$source .= "<source src='".$this->webpath($fileNoExt . $vidExt[$i])."?raw' type='".$vidType[$i]."' />" . PHP_EOL;
			}

			return $source;
		}
	}

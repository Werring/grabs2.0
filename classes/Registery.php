<?php
/**
 * Created by Werring Webdevelopment.
 * User: werring
 * Date: 12/23/11
 * Time: 13:04:25
 */
final class Registery extends stdClass
{
	/**
	 * @var Registery $instance
	 * @static
	 * Singleton instance
	 */
	private static $instance = null;

	/**
	 * @var array $object
	 * Stores Objects
	 */
	private $object;

	/**
	 * @access private
	 * @final
	 * Make sure we cant call this function outside itself (Singleton)
	 */
	private final function __construct(){

	}

	/**
	 * getInstance
	 * @static
	 * @return Registery
	 */
	public static function getInstance(){
		if(is_null(self::$instance)){
			self::$instance = new Registery();
		}
		return self::$instance;
	}

	/**
	 * __set
	 * @param string $name Name
	 * @param object $value Object
	 */

	public function __set($name,$value){
		is_string($name) or die('Name is not a String');
		is_object($value) or die('Value is not an Object');
		$this->object[$name] = $value;
	}
	/**
	 * @param string $name Name
	 * @return mixed
	 */
	public function __get($name){
		is_string($name) or die('Name is not a String');
		return $this->object[$name];
	}
}

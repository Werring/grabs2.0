<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 1/22/12
	 * Time: 18:57:19
	 */
	class Administration_Manage extends Administration_Base
	{
		const PERMISSION = Administration::MANAGE;


		private $output;

		protected function init(){
			if(Administration::checkPermission($this->reg->User->access,self::PERMISSION)) $this->manage();
			else new Administration_Default();
		}

		private function manage(){
			$this->url .=  "?manage";
			$this->output = <<<HTML
<ul id="adminSubMenu"><li><a href="{$this->url}&mod=list">List users</a></li><li>//</li><li><a href="{$this->url}&mod=create">Add user</a></li></ul>
HTML;
			$mod = isset($_GET['mod']) ? $_GET['mod']: 'list';
//*
  			switch($mod){
				case 'create':
					$this->create();
					break;
				case 'add': //Need POST data
					$this->add();
					break;
				case 'edit': //Need UserID
					
				    break;
				case 'update': //Need POST data
					break;
				case 'delete': //Need UserID
					$this->delete();
					break;
				case 'list':
				default:
					$this->ulist();
					break;
			}
/**/			$this->reg->Template->setVar('acp',$this->output);

		}

		private function ulist(){
			$table = 'Users';
			$select = '*';
			$listing = $this->reg->DB->select($table,$select);
			$edit = '<img src="/templates/default/acp/edit.gif" alt=""/>';
			$del = '<img src="/templates/default/acp/delete.gif" alt=""/>';
			$this->output.= "<table cellspacing='0'><thead><tr><th>id</th><th>Name</th><th>Email</th><th>Access</th><th>Edit</th></tr></thead><tbody>";
			for($i=0;$i<$listing['affected'];$i++){
				$id = $listing[$i]['id'];
				$name = $listing[$i]['name'];
				$email = $listing[$i]['email'];
				$access = decbin(ord($listing[$i]['access']));
				$this->output.= <<<HTML
<tr><td>$id</td><td>$name</td><td>$email</td><td class='ManageAccess'>$access</td><td>
HTML;
				if($this->reg->User->id != $id){
					$this->output .= <<<HTML
<a href="$this->url&mod=edit&id=$id">$edit</a> <a href="$this->url&mod=delete&id=$id">$del</a>
HTML;
				}
				$this->output .= '&nbsp;</td></tr>' . PHP_EOL;

			}
			$this->output.= "</tbody></table>";
		}

		private function create(){
			if(isset($_SESSION['postError']) && count($_SESSION['postError'])>0){
				$error = $_SESSION['postError'];
				$this->output.= "<p class='error'>" . implode("<br/>\n",$error) . "</p>";
			}
			unset($_SESSION['postError']);

			$this->output.=<<<HTML
<form action="$this->url&mod=add" method="POST">
<dl>
<dt>Name</dt>
<dd><input type="text" name="name"/></dd>
<dt>Email</dt>
<dd><input type="text" name="email"/></dd>
<dt>Password</dt>
<dd><input type="password" name="pass"/></dd>
<dt>Password check</dt>
<dd><input type="password" name="pass2"/></dd>
<dt>Privileges</dt>
<dd>
	<label title="Not yet implemented" style="color: grey"><input checked='checked' type="checkbox" name="priv[comment]" value="1"/> Comment</label><br/>
	<label title="Not yet implemented" style="color: grey"><input checked='checked' type="checkbox" name="priv[edit]" value="1"/> Edit</label><br/>
	<label><input type="checkbox" name="priv[upload]" checked='checked' value="1"/> Upload</label><br/>
	<label><input type="checkbox" name="priv[delete]" value="1"/> Delete</label><br/>
	<label><input type="checkbox" name="priv[manage]" value="1"/> Manage</label><br/>&nbsp;
</dd>
<dt><input type="submit" value="Create User" /></dt>
</dl>

</form>
HTML;
;
		}

		private function add(){

			if($_SERVER['REQUEST_METHOD'] !== 'POST'){
				$this->redirectMod('create');
			}
			$name       = $_POST['name'];
			$email      = filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
			$password   = md5(sha1($_POST['pass']));
			$password2  = md5(sha1($_POST['pass2']));
			$priv       = $_POST['priv'];
			$error = array();
			if(strlen($name) < 4){
				$error[] = 'Name to short';
			}
			$name = $this->reg->DB->res($name);
			if($password!=$password2){
				$error[] = 'Passwords don\'t match';
			}
			if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
				$error[] = 'Email not valid';
			} else {
				$email = $this->reg->DB->res($email);
			}
			$access = 0;
			foreach($priv as $pname=>$null){
				/** @noinspection PhpUnusedLocalVariableInspection */
				$pname = strtoupper($pname);
				$access += isset(Administration::$pname) ? Administration::$pname : 0;
			}
			//$access = "b'" . decbin($access) . "'";

			if(count($error)==0){
				$table = 'Users';
				$data = array();
				$data['name'] = $name;
				$data['email'] = $email;
				$data['password'] = $password;
				$data['access'] = $access;
				if(!$this->reg->DB->insert($table,$data)){
					$error[] = $this->reg->DB->lastError();
				}
			}
			if(count($error) > 0){
				$_SESSION['postError'] = $error;
				$this->redirectMod('create');
			} else {
				unset($_SESSION['postError']);
				$this->redirectMod('list');
			}
		}
		private function delete(){
			$id = $this->reg->DB->res($_GET['id']);
			if(isset($_GET['ok'])){
				$this->reg->DB->delete('Users',array('id'=>$id));
				$this->redirectMod('list');
			} else {

				$name = $this->reg->DB->select('Users','name','WHERE `id`="'.$id.'"');
				$username = $name[0]['name'];
				$this->output.=<<<HTML
<p class='adminText'>
	Are you sure you want to delete $username?<br/>
	<a href="$this->url&mod=delete&id=$id&ok">Yes</a> | <a href="$this->url">No</a>
</p>
HTML;

			}
		}

	}

<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 1/22/12
	 * Time: 14:59:04
	 */
	class Administration_Delete extends Administration_Base
	{
		const PERMISSION = Administration::DELETE;

		protected function init(){
			if(Administration::checkPermission($this->reg->User->access,self::PERMISSION)) $this->delete();
			else new Administration_Default();
		}

		private function delete(){
			$file = $this->reg->Conf->File->pInfo['file'];
			if(file_exists($file) && !is_dir($file))
			{
				unlink($this->reg->Conf->File->pInfo['file']);
				header('location: ' . $this->reg->Conf->File->pInfo['webPath']);
				exit();
			} else new Administration_Default();
		}
	}

<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 1/22/12
	 * Time: 18:51:35
	 */
	class Administration_Login extends Administration_Base
	{
		protected function  init(){
			$this->url .= "login";
			$mod = isset($_GET['mod']) ? $_GET['mod']: 'login';
			switch($mod){
				case 'login':
					$this->login();
					break;
				case 'logoff':
					$this->logoff();
					break;
			}

		}
		private function login(){
			if(is_null($this->reg->User->name))
			{	$loginForm = <<<HTML
<form action="?login" method="POST">
	<dl>
		<dt>Username or email</dt>
		<dd><input type="text" name="user"/></dd>
		<dt>Password</dt>
		<dd><input type="password" name="pass" /></dd>
	</dl><input type="submit" value="Login" />
</form>
HTML;

				$this->reg->Template->setVar('acp',$loginForm);
			}
			else new Administration_Default();
		}
		private function logoff(){
			$_SESSION['has_account'] = false;
			$this->reg->Template->setVar('acp','Good bye!');
			session_destroy();
			new Administration_Default();
		}
	}

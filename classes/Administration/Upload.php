<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 1/22/12
	 * Time: 18:20:59
	 */
	class Administration_Upload extends Administration_Base
	{
		const PERMISSION = Administration::UPLOAD;
		protected function init(){
			if(Administration::checkPermission($this->reg->User->access,self::PERMISSION)) {
				$mod = isset($_GET['mod']) ? $_GET['mod']: 'upload';
				switch($mod){
					case 'uploader':
						$this->uploader();
						break;
					case 'upload':
					default:
						$this->upload();
						break;
				}

			}
			else new Administration_Default();

		}

		protected function upload(){
			$html = <<<HTML
<div id='upload'>
	<noscript>Please enable Javascript to enable uploading</noscript>
</div>
<div id='links'></div>
HTML;
			$this->reg->Template->addJS('acp/fileuploader.js');
			$this->reg->Template->addJS('acp/upload.js');
			$this->reg->Template->addCSS('acp/fileuploader.css');

			$this->reg->Template->setVar('acp',$html);
		}

		protected function uploader(){
			header("HTTP/1.0 200 OK");
			// list of valid extensions, ex. array("jpeg", "xml", "bmp")
			$allowedExtensions = array();
			// max file size in bytes
			$sizeLimit = 15 * 1024 * 1024;

			$uploader = new Uploader_qqFileUploader($allowedExtensions, $sizeLimit);
			$uploadDir = __PATH__ . 'files/' . $this->reg->User->name . "/";
			if(!file_exists($uploadDir)) mkdir($uploadDir,0777);
			elseif(!is_writable($uploadDir)) chmod($uploadDir,0777);
			$result = $uploader->handleUpload($uploadDir);
			// to pass data through iframe you will need to encode all html tags
			echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
			exit();
		}
	}

<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 1/22/12
	 * Time: 15:13:41
	 */

	/**
	 * @TODO Implement File Logging
	 */
	class Administration_Base
	{
		/**
		 * @var stdClass
		 */
		protected $reg;

		/**
		 * @var #E#P#P#P#P#C\Administration_Base.reg.Conf.File.pInfo|?
		 */
		protected $url;
		/**
		 *
		 */
		public final function __construct(){
			$this->reg = Registery::getInstance();
			$this->url = $this->reg->Conf->File->pInfo['webPath'];
			$this->reg->Template->addCSS('acp/admin.css');
			$this->init();

		}
		/**
		 *
		 */
		protected function init(){
			exit('Can\'t call ' . __CLASS__ . ' directly, it should be extended. ');
		}

		/**
		 * @param $modName
		 */
		protected function redirectMod($modName=""){
			header('Location: ' . $this->url . "&mod=" . $modName);
			exit();
		}
	}

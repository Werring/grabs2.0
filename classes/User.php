<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 1/22/12
	 * Time: 15:28:04
	 */
	class User
	{
		/**
		 * @var stdClass
		 */
		protected $reg;

		protected $access;

		protected $name;

		protected $email;

		protected $password;

		protected $id;

		protected $isset;
		/**
		 * __construct
		 */
		public function __construct(){
			$this->reg      = Registery::getInstance();
			$this->isset    = false;
		}

		/**
		 * @param array $account
		 */
		public function setUser(array $account = array()){
			$this->access   = isset($account['access'])     ? $account['access']    : 0;
			$this->name     = isset($account['name'])       ? $account['name']      : 'anonamouse';
			$this->email    = isset($account['email'])      ? $account['email']     : 'no@mail.com';
			$this->password = isset($account['password'])   ? $account['password']  : md5('');
			$this->id       = isset($account['id'])         ? $account['id']        : -1;
			$this->isset    = true;
		}

		public function unsetUser(){
			$this->setUser();
			$this->isset = false;
		}
		/**
		 * @param $name
		 * @return mixed
		 */
		public function __get($name){
			$ret = null;
			if($this->isset) {
				switch($name){
					case 'access':
						$ret = $this->access;
						break;
					case 'name':
						$ret = $this->name;
						break;
					case 'email':
						$ret = $this->email;
						break;
					case 'id':
						$ret = $this->id;
						break;
				}
			}
			return $ret;
		}

		/**
		 * @param $pass
		 * @return bool
		 */
		public function isPassword($pass){
			return ((md5(sha1($pass)) == $this->password || $pass == $this->password) && $this->isset);
		}
	}

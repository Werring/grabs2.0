<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/30/11
	 * Time: 17:29:12
	 */
	/**
	 * Handle file uploads via XMLHttpRequest
	 */
	class Uploader_qqUploadedFileXhr {
		/**
		 * Save the file to the specified path
		 * @param $path
		 * @return bool
		 */
		function save($path) {
			$input = fopen("php://input", "r");
			$var = tempnam(__PATH__,'UPLOAD_');
			$temp = fopen($var,'w+');
			$realSize = stream_copy_to_stream($input, $temp);
			fclose($input);

			if ($realSize != $this->getSize()){
				return false;
			}
			$target = fopen($path, "w");
			fseek($temp, 0, SEEK_SET);
			stream_copy_to_stream($temp, $target);
			fclose($target);
			fclose($temp);
			if(file_exists($var)) unlink($var);
			return true;
		}

		/**
		 * @return mixed
		 */
		function getName() {
			return str_replace(" ","-",$_GET['qqfile']);
		}

		/**
		 * @return int
		 * @throws Exception
		 */
		function getSize() {
			if (isset($_SERVER["CONTENT_LENGTH"])){
				return (int)$_SERVER["CONTENT_LENGTH"];
			} else {
				throw new Exception('Getting content length is not supported.');
			}
		}
	}
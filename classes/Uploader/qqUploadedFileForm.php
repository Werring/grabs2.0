<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/30/11
	 * Time: 17:29:57
	 */

	/**
	 * Handle file uploads via regular form post (uses the $_FILES array)
	 */
	class Uploader_qqUploadedFileForm
	{
		/**
		 * Save the file to the specified path
		 * @param $path
		 * @return bool
		 */
		function save($path) {
			if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
				return false;
			}
			return true;
		}
		/**
		 * @return String
		 */
		function getName() {
			return str_replace(" ","-",$_FILES['qqfile']['name']);
		}
		/**
		 * @return String
		 */
		function getSize() {
			return $_FILES['qqfile']['size'];
		}

	}

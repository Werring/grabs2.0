<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/23/11
	 * Time: 16:09:49
	 */
	final class Configuration
	{
		/**
		 * $confuguration
		 * @var array
		 * @access private
		 */
		private $configuration = array();

		/**
		 * __construct
		 * @access public
		 */
		public function __construct(){
			require_once __PATH__ . 'config.php';
			$this->configuration = isset($__) ? $__ : array();
		}

		/**
		 * @param $name
		 * @return null|mixed
		 */
		public function __get($name){
			return isset($this->configuration[$name]) ? $this->configuration[$name] : null ;
		}

		/**
		 * @param string $name
		 * @param mixed $value
		 */
		public function __set($name,$value){
			$this->configuration[$name] = $value;

		}
	}

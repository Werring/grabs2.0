<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/23/11
	 * Time: 15:54:21
	 */
	class FileDisplay
	{
		/**
		 * @var bool
		 */
		protected $ok = true;

		/**
		 * @var array
		 */
		protected $tmplVars = array();

		/**
		 * @param $filename
		 * @return mixed
		 */
		final protected function file_extension($filename)
		{
			$path_info = pathinfo($filename);
			return $path_info['extension'];
		}
		/**
		 * @param string $file file
		 * @return string
		 */
		final protected function humanReadableFilesize($file) {
			if(file_exists($file)){
				$size = filesize($file);
				$mod = 1024;

				$units = explode(' ','B KB MB GB TB PB');
				for ($i = 0; $size > $mod; $i++) {
					$size /= $mod;
				}

				return round($size, 2) . ' ' . $units[$i];
			} else {
				return '-- B';
			}
		}
		/**
		 * force_download
		 */
		final protected function force_download(){
			if(file_exists($this->reg->Conf->File->pInfo['file'])){
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Disposition: attachment; filename=" .$this->reg->Conf->File->pInfo['basename']);
				header("Content-Transfer-Encoding: binary");

				$this->raw_download();
			}
			$this->display404();
		}

		/**
		 * raw_download
		 */
		final protected function raw_download(){
			if(file_exists($this->reg->Conf->File->pInfo['file'])){
				header("Content-Type: " . mime_content_type($this->reg->Conf->File->pInfo['file']));
				header("Content-Length: " . filesize($this->reg->Conf->File->pInfo['file']));
				header("Content-MD5: " . base64_encode(md5_file($this->reg->Conf->File->pInfo['file'])));
				echo file_get_contents($this->reg->Conf->File->pInfo['file']);
				exit();
			}

			$this->display404();
		}

		/**
		 * @param string $ext file extension
		 * @return string
		 */
		final protected function icon($ext){
			$list = "3gp.png,bmp.png,eps.png,gz.png,jpg.png,mp4.png,png.png,rtf.png,tmp.png,xpi.png,7Z.png,css.png,exe.png,ico.png,js.png,mpg.png,pps.png,sis.png,txt.png,zip.png,any.png,dat.png,fla.png,inf.png,jsp.png,ogg.png,ppt.png,swf.png,vob.png,asp.png,dmg.png,flv.png,jad.png,mdb.png,pdf.png,psd.png,tar.png,xls.png,avi.png,doc.png,gif.png,jar.png,mp3.png,php.png,rar.png,tif.png,xml.png";
			$list = explode(",",$list);
			if(!in_array($ext.".png",$list)){
				$ext = "any";
			}
			return $ext. ".png";
		}

		/**
		 * @var stdClass
		 */
		protected $reg;

		/**
		 * __construct
		 */
		public final function __construct(){

			$this->reg = Registery::getInstance();
			$this->reg->Conf->File = new stdClass();
			$this->reg->Conf->File->pInfo = pathinfo(__PATH__ . 'files/' . $_GET['file']);
			$this->reg->Conf->File->pInfo['file'] = __PATH__ . 'files/' . $_GET['file'];
			$this->reg->Conf->File->pInfo['webPath'] = "/" . $_GET['file'];
			$this->reg->Conf->headers = getallheaders();
			$this->ok = $this->fileCheck();


			$this->defaultVars();
			$fullRequest = __PATH__ . substr($this->reg->Conf->File->pInfo['webPath'],1);
			if(!strstr($this->reg->Conf->headers['Accept'],"html")) $this->raw_download();
			elseif(isset($_GET['raw'])) { $this->raw_download();}
			elseif(isset($_GET['download'])) $this->force_download();
			elseif($this->ok) {
				header("Content-Type: text/html; charset=UTF-8");
				$this->display();
			}
			elseif(file_exists($fullRequest)&& !is_dir($fullRequest)){
				header('Location: ' . $this->webpath($fullRequest));
				exit();
			}
			elseif(!file_exists($this->reg->Conf->File->pInfo['file'])) $this->display404();
			else $this->displayIndex();
			$this->reg->Template->fillTemplate($this->tmplVars);
		}

		/**
		 * @return bool
		 */
		final protected function fileCheck(){
			$flists = $this->reg->Conf->forbidden;
			return
				!(

					$this->in_array($this->reg->Conf->File->pInfo['basename'],$flists['file']) ||
					$this->in_array($this->reg->Conf->File->pInfo['extension'],$flists['ext']) ||
					$this->in_array($this->reg->Conf->File->pInfo['filename'],$flists['name']) ||
					is_dir($this->reg->Conf->File->pInfo['file']) ||
					!file_exists($this->reg->Conf->File->pInfo['file'])
				);
		}

		/**
		 * display()
		 */
		protected function display(){

		}

		protected function defaultVars(){
			$this->tmplVars['lbContent'] = "<a href='".$this->reg->Conf->File->pInfo['webPath']."?raw'>Display raw file<br/>in browser</a>";
			$this->tmplVars['size']= $this->humanReadableFilesize($this->reg->Conf->File->pInfo['file']);
			$this->tmplVars['file'] = $this->reg->Conf->File->pInfo['webPath'];
			$this->tmplVars['mime'] = mime_content_type($this->reg->Conf->File->pInfo['file']);
			$this->tmplVars['name'] = (strlen($this->reg->Conf->File->pInfo['basename']) > 47) ? substr($this->reg->Conf->File->pInfo['basename'],0,47) . "..." : $this->reg->Conf->File->pInfo['basename'];
			$this->tmplVars['icon'] = $this->icon($this->reg->Conf->File->pInfo['extension']);
			$this->tmplVars['ext'] = $this->reg->Conf->File->pInfo['extension'];
			$this->tmplVars['download'] = $this->reg->Template->wrapTag('a','Download', "id='downloadMenu' href='{$this->reg->Conf->File->pInfo['webPath']}?download'");
			$this->tmplVars['content'] = "<center><a href='".$this->reg->Conf->File->pInfo['webPath']."?download'><img src='http://static.werringweb.nl/icon/".$this->tmplVars['icon'] . "' /></a><br /><a href='".$this->reg->Conf->File->pInfo['webPath']."?download'>Download file</a></center>";
			$this->tmplVars['nocontent'] = "false";
			$this->tmplVars['img'] = "false";
			$this->tmplVars['txt'] = "false";
			$this->tmplVars['editor'] = "<a href='".$this->reg->Conf->File->pInfo['webPath']."?login'>Login</a>";
			$this->tmplVars['admin'] = '';
		}

		/**
		 * displayIndex()
		 */
		protected final function displayIndex(){
			$this->tmplVars['lbContent'] = '';
			$this->tmplVars['file'] = "Grabs";
			$this->tmplVars['mime'] = "WWD";
			$this->tmplVars['name'] = "WerringWebDevelopment Grabs";
			$this->tmplVars['txt'] = "true";
			$this->tmplVars['download'] = "";
			$this->tmplVars['content'] = <<<HTML
		<p>Grabs for Werring webdevelopment.<br/>Design and source by Thom Werring</p>
HTML;

		}
		/**
		 * display404()
		 */
		protected final function display404(){
			header("HTTP/1.0 404 Not Found");
			$this->tmplVars['lbContent'] = 'Sorry but this link is invalid';
			$this->tmplVars['mime'] = "404 not found";
			$this->tmplVars['name'] = (strlen("404 file not found (" . $this->reg->Conf->File->pInfo['webPath'] . ")") > 47) ? substr("404 file not found (" . $this->reg->Conf->File->pInfo['webPath'] . ")",0,47) . "..." : "404 file not found (" . $this->reg->Conf->File->pInfo['webPath'] . ")";
			$this->tmplVars['nocontent'] = "true";
			$this->tmplVars['content'] = "";
			$this->tmplVars['download'] = "";
		}

		/**
		 * @param mixed $ar1
		 * @param mixed $ar2
		 * @return bool
		 */
		private final function in_array($ar1,$ar2){
			$hit = false;
			if(!is_array($ar2)) return false;
			if(is_array($ar1)){
				for($i=0;$i<count($ar1);$i++){
					$hit = in_array($ar1[$i],$ar2);
					if($hit) break;
				}
			} else {
				$hit = in_array($ar1,$ar2);
			}
			return $hit;
		}

		/**
		 * @param $file
		 *
		 * @return mixed
		 */protected final function webpath($file){
			return str_replace(__PATH__ . "files","",$file);
		}

	}

<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/23/11
	 * Time: 15:51:10
	 */
	final class Administration
	{
		/**
		 * Binary privileges
		 * COMMENT: 00001
		 * EDIT:    00010
		 * DELETE:  00100
		 * UPLOAD:  01000
		 * MANAGE:  10000
		 */
		const COMMENT = 1;
		const EDIT    = 2;
		const DELETE  = 4;
		const UPLOAD  = 8;
		const MANAGE  = 16;

		/**
		 * @var stdClass
		 */
		private $reg;
		/**
		 * @var bool
		 */
		private $account = false;
		/**
		 * @var null|string
		 */
		private $action = null;
		/**
		 * @param $name
		 * @return mixed|bool|null
		 * Allow getting private|proteced vars as read only attributes
		 */
		public function __get($name){
			$return = null;
			switch($name){
				case 'has_account':
					$return = $this->account;
					break;
				default:
					if(isset($this->$name)){
						$return = $this->$name;
					}
					break;
			}
			return $return;
		}
		/**
		 * Constructs our admin class and starting session
		 */
		public function __construct(){
			$this->reg = Registery::getInstance();
			session_start();
			if(isset($_GET['debug'])){
				$this->reg->User->setUser();
				$acc['access'] = $this->reg->User->access;
				$acc['name']   = $this->reg->User->name;
				$acc['email']  = $this->reg->User->email;
				$acc['pass']   = md5('');
				$acc['id']     = $this->reg->User->id;
				$_SESSION['has_account'] = $this->reg->User->isset;
				$_SESSION['account'] = $acc;

			}
			if($_SESSION['has_account']===true){
				$this->account = true;
			}
			if(isset($_GET['login'])) {
				$this->action = 'login';
			}
		}
		/**
		 * Opens up data connection if posting data or if already admin
		 */
		public function run(){
			if($_POST['user']){
				$this->reg->DB->connect();
				$user = $this->reg->DB->res($_POST['user']);
				$pass = md5(sha1($_POST['pass']));
				$table  = "Users";
				$select = "*";
				$where  = "WHERE (name='$user' OR email='$user' ) AND password='$pass'";
				$result = $this->reg->DB->select($table,$select,$where);
				if($result['affected'] == 1){
					$this->account = true;
					$_SESSION['has_account'] = true;
					$_SESSION['account'] = $result[0];
					$_SESSION['account']['access'] = ord($result[0]['access']); //convert from string to bit value
					$this->reg->User->setUser($_SESSION['account']);

					$return = <<<TXT
Welcome {$result[0]['name']}.
Privs:

TXT;
					$return .= (self::checkPermission($this->reg->User->access,self::UPLOAD)) ? "- Upload" . PHP_EOL : '' ;
					$return .= (self::checkPermission($this->reg->User->access,self::MANAGE)) ? "- Manage Users" . PHP_EOL : '' ;
					$return .= (self::checkPermission($this->reg->User->access,self::COMMENT)) ? "- Comment" . PHP_EOL : '' ;
					$return .= (self::checkPermission($this->reg->User->access,self::EDIT)) ? "- Edit" . PHP_EOL : '' ;
					$return .= (self::checkPermission($this->reg->User->access,self::DELETE)) ? "- Delete" . PHP_EOL : '' ;
					$this->reg->Template->setVar('acp',nl2br($return));
				}
			}
			if($this->account || !is_null($this->action)){
				if(isset($_SESSION['account']['access']))
					$this->reg->User->setUser($_SESSION['account']);
				$this->reg->Template->addPage('acp/admin.html');
				$this->reg->Template->addJS('acp/admin.js');
				$this->reg->DB->connect();
				$this->account ? $this->runAdmin() : $this->checkAction();

			}
		}
		/**
		 * Trigger action check and
		 * fills leftbar with admin options
		 */
		private function runAdmin(){
			$this->checkAction();
			$path = $this->reg->Conf->File->pInfo['webPath'];
			$editBar  = "";
			$editBar .= (self::checkPermission($this->reg->User->access,self::UPLOAD)) ? "<a id='menuUpload' href='{$path}?upload'>Upload</a><br/>" . PHP_EOL : '' ;
			$editBar .= (self::checkPermission($this->reg->User->access,self::MANAGE)) ? "<a id='menuManage' href='{$path}?manage'>Manage Users</a><br/>" . PHP_EOL : '' ;
			if(file_exists($this->reg->Conf->File->pInfo['file']) && !is_dir($this->reg->Conf->File->pInfo['file'])){
				$editBar .= (self::checkPermission($this->reg->User->access,self::COMMENT)) ? "<a id='menuComment' href='{$path}?comment'>Comment</a><br/>" . PHP_EOL : '' ;
				$editBar .= (self::checkPermission($this->reg->User->access,self::EDIT)) ? "<a id='menuEdit' href='{$path}?edit'>Edit</a><br/>" . PHP_EOL : '' ;
				$editBar .= (self::checkPermission($this->reg->User->access,self::DELETE)) ? "<a id='menuDelete' href='{$path}?delete'>Delete</a><br/>" . PHP_EOL : '' ;
			}
			$editBar .= "<a href='{$path}?login&mod=logoff'>Logout</a><br/>" . PHP_EOL;
			$this->reg->Template->setVar('editor',$editBar);
		}

		/**
		 * check get for existing admin action
		 * if no action exist, run default
		 */
		private function checkAction(){
			$noAction = array('checkAction');

			foreach($_GET as $key => $v){
				$key = strtolower($key);
				if(array_search($key,$noAction) !== false && strlen($v)>0) continue;
				if(file_exists(__PATH__ . 'classes/Administration/' . ucfirst($key) . ".php") && $key != 'base'){
					$ACPclass = "Administration_" . ucfirst($key);
					new $ACPclass();
					$this->action = $key;
					break;
				}
			}
			if(is_null($this->action)){
				new Administration_Default();
			}
		}


		/**
		 * checkPermission
		 * Checks if user permissions allowes given permission
		 * @static
		 * @param (int) $user
		 * @param (int) $permission
		 * @return bool
		 */
		public static function checkPermission($user, $permission) {
			if($user & $permission) {
				return true;
			} else {
				return false;
			}
		}

	}

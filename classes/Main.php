<?php
/**
 * Created by Werring Webdevelopment.
 * User: werring
 * Date: 12/23/11
 * Time: 12:57:24
 */
final class Main
{
	/**
	 * @var $reg stdClass
	 */
	private $reg;
	/**
	 * __construct
	 * define all registery content
	 */
	public function __construct(){
		$this->reg = Registery::getInstance();
		$this->reg->Conf = new Configuration();
		$this->reg->DB = new Database();
		$this->reg->Template = new Template($this->reg->Conf->template);
		$this->reg->ACP = new Administration();
		$this->reg->User = new User();

	}
	/**
	 * init
	 */
	public function init(){
		$this->handleFile();

		$this->reg->Template->setVar('sitename',$this->reg->Conf->sitename);
		$this->reg->ACP->run();
		$this->reg->Template->parse();
	}

	/**
	 * handleFile()
	 */
	private function handleFile(){
		$file = __PATH__ . 'files/' .$_GET['file'];
		$sh = "file -bi '$file'";
		$mime = strtolower(shell_exec($sh));
		$pInfo = pathinfo($file);
		switch($mime){
			case 'application/x-shockwave-flash':
			case (strstr($mime,'shockwave') != false):
			case ($pInfo['extension'] == 'swf'):
				$class = 'Filetypes_SWF';
				break;
			case (strstr($mime,'text') != false):
			case (strstr($mime,'message') != false):
			case (strstr($mime,'javascript') != false):
				if(strstr($mime,'html') || strstr($mime,'xml')){
					$class = 'Filetypes_HTML';
				} elseif(strstr($mime,'php')){
					$class = 'Filetypes_PHP';
				} else {
					$class = 'Filetypes_TXT';
				}
				break;
			case strstr($mime,'image'):
				$class = 'Filetypes_IMG';
				break;
			default:
				switch($pInfo['extension']){
					case 'ogv':
					case 'mp4':
					case 'webm':
						$class = 'Filetypes_VIDEO';
						break;
					case 'swf':
						$class = 'Filetypes_SWF';
						break;
					default:
						$class = 'FileDisplay';
						break;
				}
				break;
		}

		$this->reg->FileHandler = new $class();
	}
}

<?php
	/**
	 * Created by Werring Webdevelopment.
	 * User: werring
	 * Date: 12/18/11
	 * Time: 15:12:00
	 */

	define('__PATH__',dirname(__FILE__) . DIRECTORY_SEPARATOR);
	/**
	 * @param $className
	 * Autoloader
	 */
	function __autoload($className){
		$classPath = "classes/" . (str_replace("_","/",$className) . ".php");
		if(file_exists($classPath)){
			/** @noinspection PhpIncludeInspection */
			require_once $classPath;
		}

	}
	$main = new Main();
	$main->init();

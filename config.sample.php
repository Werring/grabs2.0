<?php
        /**
         * Created by Werring Webdevelopment.
         * User: werring
         * Date: 12/23/11
         * Time: 15:59:38
         */
        $__ = array(); #Keep this line here
        #############################
        # Settings                  #
        # Uncomment lines if needed #
        #############################
          $__['template'] = 'default' ;             # Template name
          $__['sitename'] = 'Werringweb Grabs';     # Site name
          $__['database']['host']    = 'localhost'; # mysql Hostname
          $__['database']['user']    = 'root';      # mysql Username
          $__['database']['pass']    = 'DBPASS';    # mysql Password
          $__['database']['daba']    = 'Grab';      # mysql Database
          $__['database']['prefix']  = 'Grabs_';    # mysql Database
        # $__['forbidden']['ext'][]  = 'txt';       #Forbidden file (extension)
        # $__['forbidden']['name'][] = 'test';      #Forbidden file (name)
        # $__['forbidden']['file'][] = 'index.php'; #Forbidden file
